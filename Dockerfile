FROM python:3.8.5-slim-buster as base
RUN apt update && apt install -y librdkafka-dev libmaxminddb-dev && \
    rm -rf /var/lib/apt/lists/*

FROM base as builder
ENV GEOIP_COUNTRY_BASE_URL      https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-Country&license_key=
ENV GEOIP_ASN_BASE_URL      https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-ASN&license_key=
ENV GEOIP_KEY       JXBEmLjOzislFnh4&suffix



RUN apt update && apt install -y gcc curl 
WORKDIR /tmp/
RUN curl "${GEOIP_COUNTRY_BASE_URL}${GEOIP_KEY}&suffix=tar.gz" -o GeoLite2-Country.tar.gz \
    && curl  "${GEOIP_ASN_BASE_URL}${GEOIP_KEY}&suffix=tar.gz" -o GeoLite2-ASN.tar.gz

RUN tar xvf  /tmp/GeoLite2-ASN.tar.gz --strip-components 1 \
    && tar xvf  /tmp/GeoLite2-Country.tar.gz --strip-components 1 
RUN mkdir /install
WORKDIR /install
COPY requirements.txt /requirements.txt
RUN pip install --prefix=/install -r /requirements.txt

FROM base
COPY --from=builder /tmp/GeoLite2-ASN.mmdb /tmp/GeoLite2-Country.mmdb /app/
COPY --from=builder /install /usr/local
COPY src /app
WORKDIR /app
CMD ["python", "-u", "./main.py"]