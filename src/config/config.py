
from dynaconf import Dynaconf

settings = Dynaconf(
    envvar_prefix="CYBORG",
    settings_files=['settings.json', '.secrets.json'],
)
