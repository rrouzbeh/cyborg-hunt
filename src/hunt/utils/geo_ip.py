import logging


def geo_ip(asn_db, country_db, ip):
    try:
        asn_result = asn_db.get(ip)
        country_result = country_db.get(ip)
        return (asn_result['autonomous_system_number'],
                asn_result['autonomous_system_organization'],
                country_result['country']['iso_code'])
    except Exception as exp:
        return (0, "", "")
