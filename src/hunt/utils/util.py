import orjson as json
import ciso8601


def time_to_epoch(ts):
    dt = ciso8601.parse_datetime(ts)
    return (dt, dt.date())


def conn_state_enrichment(state):
    states_desc = {
        "S0": "Connection attempt seen, no reply",
        "S1": "Connection established, not terminated",
        "S2": "Established, Orig attempts close, no reply from Resp",
        "S3": "Established, Resp attempts close, no reply from Orig",
        "SF": "Normal establish & termination",
        "REJ": "Connection attempt rejected",
        "RSTO": "Established, Orig aborted (RST)",
        "RSTR": "Established, Resp aborted (RST)",
        "RSTOS0": "Orig sent SYN then RST; no Resp SYN-ACK",
        "RSTRH": "Resp sent SYN-ACK then RST; no Orig SYN",
        "SH": "Orig sent SYN then FIN; no Resp SYN-ACK (half-open)",
        "SHR": "Resp sent SYN-ACK then FIN; no Orig SYN",
        "OTH": "No SYN, not closed, midstream traffic, partial connection"
    }
    return states_desc[state]
