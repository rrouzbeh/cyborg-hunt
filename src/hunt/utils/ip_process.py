
def ip_process(ip):

    if ":" not in ip and len(ip) <= 15 and len(ip) >= 7:
        if ip.startswith("10."):
            return ("IPv4", "RFC_1918", "false", "private")

        elif ip.startswith("192.168."):
            return ("IPv4", "RFC_1918", "false", "private")

        elif ip.startswith("169.254"):
            return ("IPv4", "RFC_3927", "false", "local")

        elif ip.startswith("127.0"):
            return ("IPv4", "RFC_1122-3.2.1.3", "false", "loopback")

        elif ip.startswith("0."):
            return ("IPv4", "RFC_1700", "false",
                    "reserved_as_a_source_address_only")

        elif ip.startswith("192.88.99."):
            return ("IPv4", "RFC_3068", "false", "6to4")

        elif ip.startswith("192.31.196."):
            return ("IPv4", "RFC_3068", "false", "as112-v4")

        elif ip.startswith("192.52.193."):
            return ("IPv4", "RFC_7450", "false", "amt")

        elif ip.startswith("172."):
            octet = ip.split(".")
            if int(octet[1]) >= 16 and int(octet[1]) <= 31:
                return ("IPv4", "RFC_1918", "false", "private")
            else:
                return ("IPv4", "RFC_1366", "true", "public")

        elif ip.startswith("100."):
            octet = ip.split(".")
            if int(octet[1]) >= 64 and int(octet[1]) <= 127:
                return ("IPv4", "RFC_1918", "false", "private")
            else:
                return ("IPv4", "RFC_1366", "true", "public")

        elif ip.startswith("2"):
            octet = ip.split(".")
            if ip == "255.255.255.255":
                return ("IPv4", "RFC_8190", "false", "broadcast")
            elif int(octet[0]) >= 244:
                return ("IPv4", "RFC_1112", "false", "multicast")
            else:
                return ("IPv4", "RFC_1366", "true", "public")

        else:
            return ("IPv4", "RFC_1366", "true", "public")

    else:
        return ("IPv6", "n/a", "false", "n/a")
