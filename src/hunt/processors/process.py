from hunt.utils.ip_process import ip_process
from hunt.utils.geo_ip import geo_ip


class Process():

    def __init__(self, parser=None):
        self.obj = None
        self.parser = parser

    def enrich(self, asn_db, country_db):
        self.obj.src_ip_version, self.obj.src_ip_rfc, self.obj.src_ip_public, \
            self.obj.src_ip_type = ip_process(
                self.obj.src_ip_addr
            )

        if self.obj.src_ip_type == "public":
            self.obj.src_asn, self.obj.src_asn_org, self.obj.src_country = \
                geo_ip(
                    asn_db, country_db,
                    self.obj.src_ip_addr
                )
        else:
            self.obj.src_asn, self.obj.src_asn_org, self.obj.src_country = (
                0, "", ""
            )

        self.obj.dst_ip_version, self.obj.dst_ip_rfc, self.obj.dst_ip_public, \
            self.obj.dst_ip_type = ip_process(
                self.obj.dst_ip_addr
            )

        if self.obj.dst_ip_type == "public":
            self.obj.dst_asn, self.obj.dst_asn_org, self.obj.dst_country = \
                geo_ip(
                    asn_db, country_db,
                    self.obj.dst_ip_addr
                )
        else:
            self.obj.dst_asn, self.obj.dst_asn_org, self.obj.dst_country = (
                0, "", ""
            )
