from hunt.processors.process import Process
from hunt.utils.util import time_to_epoch


class DNS(object):
    """
    DNS records

    Args:
        ts (str): Datetime
        uid (str): Zeek UID
        src_ip_addr (str)
        src_port (int)
        dst_ip_addr (str)
        dst_port (int)
        query (str): DNS Query
        answers ([]str): DNs Response
        protocol (str)
        qtype_name (str)
        qclass_name (str)
        qtype (int)
        qclass (int)
        RA (bool)
        trans_id (int)
        rcode (int)
        rtt (float)
        Z (int)
        AA (bool)
        rejected (bool)
        RD (bool)
        TC (bool)
        rcode_name (str)
        TTLs ([]float)

    """

    def __init__(
            self, ts, uid, src_ip_addr="", src_port=0, dst_ip_addr="",
            dst_port=0, query="", answers=[""], protocol="", qtype_name="",
            qclass_name="", qtype=0, qclass=0, RA=False, trans_id=0,
            rcode=0, rtt=False, Z=0, AA=False, rejected=False, RD=False,
            TC=False, rcode_name="", TTLs=[0.0], network_community_id=""
    ):

        self.event_date_creation, self.date = time_to_epoch(ts)
        self.uid = uid
        self.src_ip_addr = src_ip_addr
        self.src_port = src_port
        self.dst_ip_addr = dst_ip_addr
        self.dst_port = dst_port
        self.query = query
        self.answers = answers
        self.protocol = protocol
        self.qtype_name = qtype_name
        self.qclass_name = qclass_name
        self.qtype = qtype
        self.qclass = qclass
        self.ra = RA
        self.trans_id = trans_id
        self.rcode = rcode
        self.rtt = rtt
        self.z = Z
        self.aa = AA
        self.rejected = rejected
        self.rd = RD
        self.tc = TC
        self.rcode_name = rcode_name
        self.ttls = TTLs
        self.network_community_id = network_community_id


class DNSProcessor(Process):
    def make(self, obj):
        self.obj = DNS(**obj)

    def enrich(self, asn_db, country_db):
        self.obj.subdomain, self.obj.domain, self.obj.domain_suffix, \
            self.obj.domain_name = self.parser.extract(
                self.obj.query
            )
        self.obj.subdomain_length = len(self.obj.subdomain)
        self.obj.domain_length = len(self.obj.domain)

        super().enrich(asn_db, country_db)
