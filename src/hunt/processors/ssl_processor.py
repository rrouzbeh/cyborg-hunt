from hunt.processors.process import Process
from hunt.utils.util import time_to_epoch


class SSL(object):
    """
    SSL records

    Args:
        ts (str): Datetime
        uid (str): Zeek UID
        src_ip_addr (str)
        src_port (int)
        dst_ip_addr (str)
        dst_port (int)
        server_name (str)
        client_subject (str)
        issuer (str)
        curve (str)
        ja3s (str)
        ja3 (str)
        client_issuer (str)
        version (str)
        tags (str)
        next_protocol (str)
        subject (str)
        cert_chain_fuids (str[])
        client_cert_chain_fuids (str[])
        established (int)
        validation_status (str)
        last_alert (str)
        cipher (str)
        resumed (int)
        uid (str)
        network_community_id (str)

    """

    def __init__(
            self, ts, uid, src_ip_addr="", src_port=0, dst_ip_addr="",
            dst_port=0, server_name="", client_subject="",
            issuer="", curve="", ja3s="",
            ja3="", client_issuer="", version="",
            tags="", next_protocol="", subject="",
            cert_chain_fuids=[""], client_cert_chain_fuids=[""],
            established=0, validation_status="", last_alert="",
            cipher="", resumed=0, network_community_id=""
    ):

        self.event_date_creation, self.date = time_to_epoch(ts)
        self.uid = uid
        self.src_ip_addr = src_ip_addr
        self.src_port = src_port
        self.dst_ip_addr = dst_ip_addr
        self.dst_port = dst_port
        self.server_name = server_name
        self.client_subject = client_subject
        self.issuer = issuer
        self.curve = curve
        self.ja3s = ja3s
        self.ja3 = ja3
        self.client_issuer = client_issuer
        self.version = version
        self.tags = tags
        self.next_protocol = next_protocol
        self.subject = subject
        self.cert_chain_fuids = cert_chain_fuids
        self.client_cert_chain_fuids = client_cert_chain_fuids
        self.established = established
        self.validation_status = validation_status
        self.last_alert = last_alert
        self.cipher = cipher
        self.resumed = resumed
        self.network_community_id = network_community_id


class SSLProcessor(Process):
    def make(self, obj):
        self.obj = SSL(**obj)
