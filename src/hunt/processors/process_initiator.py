from hunt.processors import ConnProcessor, \
    DNSProcessor, \
    HTTPProcessor,  \
    SSLProcessor
from fasttld import FastTLDExtract


def process_initiator(log_type):
    parser = FastTLDExtract()

    processors = {
        "conn": (ConnProcessor(), "date, event_date_creation, src_ip_addr,"
                 "src_ip_rfc, src_ip_public, src_ip_type, src_ip_version,"
                 "src_port, src_asn_org, src_asn, src_country, dst_ip_addr,"
                 "dst_ip_rfc, dst_ip_public, dst_ip_type, dst_ip_version,"
                 "dst_port, dst_country, dst_asn_org, dst_asn, protocol,"
                 "service_name, duration, bytes_out, bytes_out_ip, bytes_in,"
                 "bytes_in_ip, bytes_missed, packets_in, packets_out,"
                 "src_is_local, dst_is_local, uid, network_community_id,"
                 "conn_state, conn_state_desc, history, version, src_l2_addr,"
                 "dst_l2_addr, sensor_node_name, sensor_interface"),
        "dns": (DNSProcessor(parser), "date, event_date_creation, src_ip_addr,"
                "src_ip_rfc, src_ip_public, src_ip_type, src_ip_version, "
                "src_port, src_asn_org, src_asn, src_country, dst_ip_addr,"
                "dst_ip_rfc, dst_ip_public, dst_ip_type, dst_ip_version, "
                "dst_port, dst_country, dst_asn_org, dst_asn, protocol,"
                "query, subdomain, domain, domain_suffix, domain_name, "
                "subdomain_length, domain_length, answers, qtype_name,"
                "qclass_name, qtype, qclass, ra, trans_id, rcode, rtt, "
                "z, aa, rejected, rd, tc, rcode_name, ttls, uid,"
                "network_community_id"),
        "http": (HTTPProcessor(), "date, event_date_creation, src_ip_addr,"
                 "src_ip_type, src_ip_rfc, src_ip_public, src_ip_version,"
                 "src_port, src_country, src_asn_org, src_asn, dst_ip_addr,"
                 "dst_ip_type, dst_ip_rfc, dst_ip_public, dst_ip_version,"
                 "dst_port, dst_country, dst_asn_org, dst_asn, host,"
                 "bytes_out, bytes_in, status_code, http_referrer,"
                 "http_user_agent, http_user_agent_length, post_body,"
                 "uri_lenght, uri, src_mime_types, dst_mime_types, dst_fuids,"
                 "trans_depth, post_username, http_content_type, status_msg,"
                 "dst_filenames, info_code, src_fuids, version, origin, uid,"
                 "tags, username, http_method, info_msg, proxied,"
                 "network_community_id"),
        "ssl": (SSLProcessor(), "date, event_date_creation, src_ip_addr,"
                "src_ip_type, src_ip_rfc, src_ip_public, src_ip_version,"
                "src_port, src_country, src_asn_org, src_asn, dst_ip_addr,"
                "dst_ip_type, dst_ip_rfc, dst_ip_public, dst_ip_version,"
                "dst_port, dst_country, dst_asn_org, dst_asn, server_name,"
                "client_subject, issuer, curve, ja3s, ja3, client_issuer,"
                "version, tags, next_protocol, subject, cert_chain_fuids,"
                "client_cert_chain_fuids, established, validation_status,"
                "last_alert, cipher, resumed, uid, network_community_id")
    }

    return processors.get(log_type)
