from hunt.processors.process import Process
from hunt.utils.util import conn_state_enrichment
from hunt.utils.util import time_to_epoch


class Conn(object):
    """
    Conn records

    Args:
        ts (str): Datetime
        uid (str): Zeek UID
        src_ip_addr (str)
        src_port (int)
        dst_ip_addr (str)
        dst_port (int)
        protocol (str)
        service_name (str): Service name [http,dns,smtp,...]
        conn_state (str): A summarized state of each connection
        history (str): Conn state history
        src_is_local (bool)
        dst_is_local (bool)
        bytes_missed (int) Number of bytes missing(packet loss)
        packet_out (int) Number of packet originator sent
        packets_in (int) Number of packet responder sent
        bytes_out_ip (int)  Number of originator ip bytes
        bytes_in_ip (int)  Number of responder ip bytes
        bytes_out (int) Number of originator bytes
        bytes_in (int) Number of responder bytes
        duration (float) How long connection lasted
        orig_l2_addr (str) Source MAC address
        resp_l2_addr (str) Destination MAC address
        sensor_interface (str) Zeek interface name
        sensor_node_name (str) Zeek Hostname

    """

    def __init__(
            self, ts, uid="",
            src_ip_addr="", src_port=0, dst_ip_addr="",
            dst_port=0, protocol="", service_name="",
            conn_state="", history="", version="", src_is_local="",
            dst_is_local="", bytes_missed=0, packets_out=0,
            packets_in=0, bytes_out_ip=0, bytes_in_ip=0,
            bytes_out=0, bytes_in=0, duration=0.0,
            src_l2_addr="", dst_l2_addr="", sensor_interface="",
            sensor_node_name="", network_community_id="", tunnel_parents=""
    ):

        self.event_date_creation, self.date = time_to_epoch(ts)
        self.uid = uid
        self.src_ip_addr = src_ip_addr
        self.src_port = src_port
        self.dst_ip_addr = dst_ip_addr
        self.dst_port = dst_port
        self.protocol = protocol
        self.service_name = service_name
        self.conn_state = conn_state
        self.history = history
        self.version = version
        self.src_is_local = src_is_local
        self.dst_is_local = dst_is_local
        self.bytes_missed = bytes_missed
        self.packets_out = packets_out
        self.packets_in = packets_in
        self.bytes_out_ip = bytes_out_ip
        self.bytes_in_ip = bytes_in_ip
        self.bytes_out = bytes_out
        self.bytes_in = bytes_in
        self.duration = duration
        self.src_l2_addr = src_l2_addr
        self.dst_l2_addr = dst_l2_addr
        self.network_community_id = network_community_id
        self.sensor_interface = sensor_interface
        self.sensor_node_name = sensor_node_name
        self.tunnel_parents = tunnel_parents


class ConnProcessor(Process):

    def make(self, obj):
        self.obj = Conn(**obj)

    def enrich(self, asn_db, country_db):
        self.obj.conn_state_desc = conn_state_enrichment(
            self.obj.conn_state)
        super().enrich(asn_db, country_db)
