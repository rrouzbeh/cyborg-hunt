from hunt.processors.process import Process
from hunt.utils.util import time_to_epoch


class HTTP(object):
    """
    HTTP records

    Args:
        ts (str): Datetime
        src_ip_addr (str)
        src_port (int)
        dst_ip_addr (str)
        dst_port (int)
        host (str)
        bytes_out (int)
        bytes_in (int)
        status_code (int)
        http_referrer (str)
        http_user_agent (str)
        post_body (str)
        uri (str)
        orig_mime_types ([]str)
        resp_mime_types ([]str)
        resp_fuids ([]str)
        trans_depth (int)
        post_username (str)
        http_content_type ([]str)
        status_msg (str)
        resp_filenames ([]str)
        info_code (int)
        orig_fuids ([]str)
        version (str)
        origin (str)
        uid (str)
        tags ([]str)
        username (str)
        http_method (str)
        info_msg (str)
        proxied ([]str)
        network_community_id (str)

    """

    def __init__(
            self, ts, uid, src_ip_addr="", src_port=0, dst_ip_addr="",
            dst_port=0, host="", bytes_out=0, bytes_in=0, status_code=0,
            http_referrer="", http_user_agent="", post_body="", uri="",
            orig_mime_types=[""], resp_mime_types=[""], resp_fuids=[""],
            trans_depth=0, post_username="", http_content_type=[""],
            status_msg="", resp_filenames=[""], info_code=0,
            orig_fuids=[""], version="", origin="",
            tags=[""], username="", http_method="",
            info_msg="", proxied=[""], network_community_id=""
    ):

        self.event_date_creation, self.date = time_to_epoch(ts)
        self.event_date_creation, self.date = time_to_epoch(ts)
        self.uid = uid
        self.src_ip_addr = src_ip_addr
        self.src_port = src_port
        self.dst_ip_addr = dst_ip_addr
        self.dst_port = dst_port
        self.host = host
        self.bytes_out = bytes_out
        self.bytes_in = bytes_in
        self.status_code = status_code
        self.http_referrer = http_referrer
        self.http_user_agent = http_user_agent
        self.post_body = post_body
        self.uri = uri
        self.src_mime_types = orig_mime_types
        self.dst_mime_types = resp_mime_types
        self.dst_fuids = resp_fuids
        self.trans_depth = trans_depth
        self.post_username = post_username
        self.http_content_type = http_content_type
        self.status_msg = status_msg
        self.dst_filenames = resp_filenames
        self.info_code = info_code
        self.src_fuids = orig_fuids
        self.version = version
        self.origin = origin
        self.tags = tags
        self.username = username
        self.http_method = http_method
        self.info_msg = info_msg
        self.proxied = proxied
        self.network_community_id = network_community_id


class HTTPProcessor(Process):
    def make(self, obj):
        self.obj = HTTP(**obj)

    def enrich(self, asn_db, country_db):
        print
        self.obj.http_user_agent_length = len(self.obj.http_user_agent)
        self.obj.uri_lenght = len(self.obj.uri)

        super().enrich(asn_db, country_db)
