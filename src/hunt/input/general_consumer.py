import datetime
import sys
import time

from confluent_kafka import KafkaError
import maxminddb
from maxminddb import MODE_MMAP_EXT
import orjson as json


from config.config import settings
from clickhouse_driver import Client
from hunt.logger.logger import logger


class GeneralConsumer():

    def __init__(self, consumer, processor, data_field, part):
        self.consumer = consumer
        self.processor = processor
        self.msg = None
        self.obj = None
        self.batch = []
        self.data_field = data_field
        self.asn_db = maxminddb.open_database(
            "./GeoLite2-ASN.mmdb", MODE_MMAP_EXT)
        self.country_db = maxminddb.open_database(
            "./GeoLite2-Country.mmdb", MODE_MMAP_EXT)
        self.log_type = settings.get('LOG_TYPE')
        self.db_name = settings.get('DB_NAME')
        self.clickhouse_server = settings.get('CLICKHOUSE_SERVER')
        self.batch_size = settings.get('BATCH_SIZE')
        self.batch_timeout = settings.get('BATCH_TIMEOUT')

    def insert(self):
        try:
            client = Client(host=self.clickhouse_server)
            client.execute(
                f"INSERT INTO {self.db_name}.{self.log_type} "
                f"({self.data_field}) VALUES",
                self.batch, types_check=False
            )

        except Exception as e:
            logger.error(f"Exception with insert into Clickhouse: {e}")
            sys.exit(1)

    def prepare(self, obj):
        self.batch.append(obj)

    def process_msg(self):
        self.processor.make(json.loads(self.msg.value()))
        self.processor.enrich(self.asn_db, self.country_db)
        self.obj = self.processor.obj.__dict__

    def consume(self):
        batch_start_time = datetime.datetime.now()
        while True:
            try:
                self.msg = self.consumer.poll(1.0)
                if self.msg is None:
                    logger.debug("Kafka Consumer Timeout")
                    time.sleep(0.1)
                    continue
                elif not self.msg.error():
                    self.process_msg()
                    self.prepare(self.obj)

                    batch_duration = datetime.datetime.now() - batch_start_time
                    if len(self.batch) >= self.batch_size or \
                            batch_duration.seconds >= self.batch_timeout:
                        self.insert()
                        self.consumer.commit()
                        logger.debug(
                            f"In {batch_duration} Seconds Insert "
                            f"{len(self.batch)} Logs Successfully "
                            "to Clickhouse"
                        )
                        self.batch = []
                        batch_start_time = datetime.datetime.now()
                elif self.msg.error().code() == \
                        KafkaError._PARTITION_EOF:
                    time.sleep(0.1)
                    continue
                else:
                    logger.error(f"Kafka error: {self.msg.error().str()}")
                    break

            except KeyboardInterrupt:
                self.consumer.close()
                break
