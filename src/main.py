from confluent_kafka import Consumer, TopicPartition
from multiprocessing import Process

from config.config import settings
from hunt.input.general_consumer import GeneralConsumer
from hunt.logger.logger import logger
from hunt.processors.process_initiator import process_initiator


topic = settings.get('KAFKA_TOPIC')
consumer_config = {
    'bootstrap.servers': settings.get('KAFKA_BOOTSTRAP_SERVERS'),
    'group.id': settings.get('KAFKA_GROUP_ID'),
    'session.timeout.ms': 6000,
    'enable.auto.commit': False,
    'default.topic.config': {
        'auto.offset.reset': "earliest"
    }
}


def main(part):
    logger.info(f"Start Consuming Partition: {part}")

    c = Consumer(consumer_config)
    processor, data_field = process_initiator(settings.get('LOG_TYPE'))

    _topic = TopicPartition(topic, part)
    c.assign([_topic])
    consumer = GeneralConsumer(
        c, processor, data_field, part
    )
    consumer.consume()


if __name__ == "__main__":
    logger.info("START!")
    c = Consumer(consumer_config)
    filtered_topics = c.list_topics(topic, timeout=0.2)
    partitions_dict = filtered_topics.topics[topic].partitions
    workers = {}
    num_workers = len(partitions_dict)
    while True:
        num_alive = len([w for _, w in workers.items() if w.is_alive()])
        if num_workers == num_alive:
            continue
        for part in partitions_dict.keys():
            if workers.get(part) and workers.get(part).is_alive():
                continue
            else:
                p = Process(target=main, args=(int(part),))
                p.start()
                workers[part] = (p)
